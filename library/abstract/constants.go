package abstract

const(
	// to debug or not to debug, that is the question
	DEBUG = false

	// this token stands for stdin
	STDIN_TOKEN = "-"

	// global setting for FADVICE_DONTNEED before reading
	SET_FILE_ADVICE_DONTNEED = true
)
